d3.tsv("data.tsv", function(error, data) {
  if (error) throw error;

  data.forEach(function(d) {
      d["LayerI[17-E]"] = +d["LayerI[17-E]"];
      d["LayerI[19-P]"] = +d["LayerI[19-P]"];
      d["LayerV-VI[2-E]"] = +d["LayerV-VI[2-E]"];
      d["LayerII-IV[15-P]"] = +d["LayerII-IV[15-P]"];
      d["LayerV-VI[3-E]"] = +d["LayerV-VI[3-E]"];
      d["LayerII-IV[1-P]"] = +d["LayerII-IV[1-P]"];
      d["LayerII-IV[4-P]"] = +d["LayerII-IV[4-P]"];
      d["LayerV-VI[5-E]"] = +d["LayerV-VI[5-E]"];
      d["LayerV-VI[13-E]"] = +d["LayerV-VI[13-E]"];
      d["LayerV-VI[18-P]"] = +d["LayerV-VI[18-P]"];
      d["LayerV-VI[7-E]"] = +d["LayerV-VI[7-E]"];
      d["LayerV-VI[12-P]"] = +d["LayerV-VI[12-P]"];
      d["SVZ1(migrating)[4-E]"] = +d["SVZ1(migrating)[4-E]"];
      d["SVZ1(migrating)[2-P]"] = +d["SVZ1(migrating)[2-P]"];
      d["SVZ2(migrating)[8-P]"] = +d["SVZ2(migrating)[8-P]"];
      d["Int1[1-E]"] = +d["Int1[1-E]"];
      d["Int1[5-P]"] = +d["Int1[5-P]"];
      d["Int3[11-P]"] = +d["Int3[11-P]"];
      d["Int4[6-P]"] = +d["Int4[6-P]"];
      d["Int2[12-E]"] = +d["Int2[12-E]"];
      d["Int2[14-P]"] = +d["Int2[14-P]"];
      d["Thalamus[19-E]"] = +d["Thalamus[19-E]"];
      d["Striatal_inh1[16-E]"] = +d["Striatal_inh1[16-E]"];
      d["Striatal_inh1[3-P]"] = +d["Striatal_inh1[3-P]"];
      d["Striatal_inh2[9-E]"] = +d["Striatal_inh2[9-E]"];
      d["Striatal_inh2[7-P]"] = +d["Striatal_inh2[7-P]"];
      d["Ganglionic_eminence[6-E]"] = +d["Ganglionic_eminence[6-E]"];
      d["Ganglionic_eminence[9-P]"] = +d["Ganglionic_eminence[9-P]"];
      d["SVZ3(proliferating)[15-E]"] = +d["SVZ3(proliferating)[15-E]"];
      d["RG2[14-E]"] = +d["RG2[14-E]"];
      d["RG3(cortical hem)[21-E]"] = +d["RG3(cortical hem)[21-E]"];
      d["SVZ2(VZ-SVZ)[11-E]"] = +d["SVZ2(VZ-SVZ)[11-E]"];
      d["RG1[8-E]"] = +d["RG1[8-E]"];
      d["RG4[10-E]"] = +d["RG4[10-E]"];
      d["Astrocyte_IPCs_1[10-P]"] = +d["Astrocyte_IPCs_1[10-P]"];
      d["Astrocyte_IPCs_2[13-P]"] = +d["Astrocyte_IPCs_2[13-P]"];
      d["Oligodendrocytes[16-P]"] = +d["Oligodendrocytes[16-P]"];
      d["Choroid_plexus[22-E]"] = +d["Choroid_plexus[22-E]"];
      d["Choroid_plexus[20-P]"] = +d["Choroid_plexus[20-P]"];
      d["Microglia[20-E]"] = +d["Microglia[20-E]"];
      d["Microglia[22-P]"] = +d["Microglia[22-P]"];
      d["Endothelial[18-E]"] = +d["Endothelial[18-E]"];
      d["Endothelial2[21-P]"] = +d["Endothelial2[21-P]"];
      d["Endothelial1[17-P]"] = +d["Endothelial1[17-P]"];
  });

  let width = +d3.select("#length").node().offsetWidth;
  let height = 500;

  createBar(width, height);
  drawBar(data, "");

  d3.select("#submit").on("click", function(){
    let gene = d3.select("#name").node().value;
    drawBar(data, gene);  
    });

  d3.select("#name").on("keypress", function(){
    if(d3.event.keyCode === 13) {
      let gene = d3.select("#name").node().value;
      drawBar(data, gene); 
    }
  });

  d3.select("#download").on('click', function(){
    let filename = d3.select("#name").node().value + '.png';
    saveSvgAsPng(document.getElementById("bar"), filename, {backgroundColor: "#FFFFFF"});
  });

  d3.select("#bar")
    .on("mousemove touchmove", updateTooltip);

  function updateTooltip() {
      let tooltip = d3.select(".tooltip");
      let barname = d3.select(d3.event.target).attr("name");
      let barvalue = d3.select(d3.event.target).attr("value");
      let barclass = d3.select(d3.event.target).attr("class");
      let isBar = ["bar-lay", "bar-svz", "bar-int", "bar-striatal", "bar-rg", "bar-cho"].includes(barclass);

      tooltip
          .style("opacity", +isBar)
          .style("left", (d3.event.pageX - tooltip.node().offsetWidth / 2 - 5) + "px")
          .style("top", (d3.event.pageY - tooltip.node().offsetHeight - 10) + "px");

      tooltip
        .html(barname + "<br>" + barvalue);
  }

})