function createBar(width, height){
  let margin = {top:30, right:30, bottom:30, left:30};
  let bar = d3.select("#bar")
                  .attr("width", width + margin.left + margin.right)
                  .attr("height", height + margin.top + margin.bottom)
                  .attr("transform", "translate(" + (10) + "," + (0) + ")");

  bar.append("g")
      .classed("bar-x-axis", true);

  bar.append("g")
      .classed("bar-y-axis", true);

  bar.append("g")
      .classed("centerline", true);

  bar.append("text")
      .attr("x", width / 2)
      .attr("y", 20)
      .attr("font-size", "1.3em")
      .style("text-anchor", "middle")
      .classed("bar-title", true);

  // y axis
  bar.append("text")
    .attr("transform", "rotate(-90)")
    .attr("x", -height/7)
    .attr("dy", "1em")
    .style("text-anchor", "end")
    .attr("font-size", '1.15em')
    .text("Log-normalized expression");

  // legend
  let legend_color = ["#458B00", "#FF0000", "#808080", "#0000FF", "#000000"];
  let color = d3.scaleOrdinal(legend_color);
  let legend_item = ["Layer-specific", "Interneurons", "Non-cortical", "Progenitor-like", "Non-neuronal"];

  var legend = bar.selectAll(".legend")
      .data(legend_item)
      .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) { return "translate(10," + (100 + i * 20) + ")"; });

  legend.append("rect")
      .attr("x", width - 18)
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", color);

  legend.append("text")
      .attr("x", width - 24)
      .attr("y", 9)
      .attr("dy", ".35em")
      .style("text-anchor", "end")
      .text(function(d) { return d; });
}

function drawBar(data, gene){
  let bar = d3.select("#bar");
  let padding = {
    top: 35,
    right: 60,
    bottom: 35,
    left: 10
  };
  
  let column = Array.from(data.columns);
  let xray = column.slice(1,column.length);

  let width = +bar.attr("width");
  let height = +bar.attr("height");

  if (gene === '') {gene = '0610007N19Rik';}

  let barData = data.filter(d => d.Gene.toLowerCase() === gene.toLowerCase());

  if (barData.length === 0) {
      alert("Gene not detected or not found. Please try another gene.");
      d3.select("#name").node().value = '';
      gene = '0610007N19Rik';
      barData = data.filter(d => d.Gene.toLowerCase() === gene.toLowerCase());
  }
  
  if (d3.select("#name").node().value != '') {
    d3.select("#name").node().value = barData[0].Gene;
  }

  let singleRowData = Object.values(barData[0]);
  let pureNum = singleRowData.slice(1, singleRowData.length);
  let yRange = d3.extent(pureNum);

  let singleRowIndex = Object.keys(barData[0]);
  let pureName = singleRowIndex.slice(1, singleRowIndex.length);
  // Transform data to {'base': A, 'value': B}
  let constructData = [];

  for(let i = 0; i < pureName.length; i++) {
      constructData.push({'base':pureName[i], 'value':pureNum[i]});
  };


  let center = d3.scaleLinear().range([padding.left, width - padding.right * 4]);

  let xScale = d3.scalePoint()
                 .domain(xray)
                 .rangeRound([padding.left, width - padding.right * 4])
                 .padding(0.05)
                 .align(0.1);
  let xAxis = d3.axisBottom()
                .scale(xScale)
                .ticks(45);

  let yScale = d3.scaleLinear()
                 .domain(yRange)
                 .range([height, padding.top + 200]);


  let yAxis = d3.axisLeft(yScale);

  let centerLine = d3.axisTop(center).ticks(0);

  bar.select(".bar-title")
     .text("Gene: " + barData[0].Gene);

  //Add center line
  bar.select(".centerline")
     .attr("transform", "translate(" + (50) + "," + (yScale(0) - 200) + ")")
     .transition()
     .call(centerLine);

  //Add the x Axis
  bar.select(".bar-x-axis")
      .attr("transform","translate("+ (50) +","+(height - 200)+")")
      .transition()
      .call(xAxis)
    .selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", ".15em")
        .attr("font-size", "1.5em")
        .attr("transform", "rotate(-90)");

  //Add the Y Axis
  bar.select(".bar-y-axis")
     .attr("font-size", "0.8em")
     .attr("transform","translate("+ (60) +","+(-height + 360)+")")
     .transition()
     .call(yAxis);

  var t = d3.transition()
            .duration(600)
            .ease(d3.easeBounceOut);
  
  //draw bar
  let bars = bar
                .selectAll(".bar")
                .data(constructData);

  bars
    .exit()
    .remove();

  barEnter = bars
                .enter()
                .append('g')
                .classed("bar", true);

  barEnter.append("rect");
  barEnter.append("text");

  bars = barEnter.merge(bars);

  bars.select("rect")
        .attr('class', d =>findColor(d.base.split("[")[0]))
        .attr("x", d => xScale(d.base) + 50)
        .attr('width', 15)
        .attr('name', d => d.base)
        .attr('value', d => d.value)
        .transition(t)
        .delay((d, i) => i * 30)
        .attr("y", d => yScale(Math.max(0, isNaN(d.value) ? 0 : d.value)) - 200)
        .attr("height", d => Math.abs(yScale(isNaN(d.value) ? 0 : d.value) - yScale(0)));

  bars.select("text")
      .attr("text-anchor", "middle")
      .attr("font-size", "0.75em")
      .attr("x", d => xScale(d.base) + 58)
      .attr("y", d => yScale(Math.max(0, isNaN(d.value) ? 0 : d.value)) - 200)
      .text(function(d){
        if (isNaN(d.value)) {
          return 'NA';
        }
      });

  function findColor(word){
      if (word.slice(0,5) === 'Layer') {
          return 'bar-lay';
      } else {
          if (word.slice(0, 3) === 'Int') {
              return 'bar-int';
          } else {
              if (word.split("(")[1] === "migrating)") {
                  return 'bar-svz';
              } else {
                  if (['Thal', 'Stri', 'Gang'].includes(word.slice(0, 4))) {
                      return 'bar-striatal';
                  } else {
                      if (['Olig', 'Chor', 'Micr', 'Endo'].includes(word.slice(0, 4))) {
                          return 'bar-cho';
                      } else {
                          return 'bar-rg';
                      }
                  }
              }
          }
      }

  }
}